# Remove batches of lines from the shell history

## Purpose

This is a BASH script which takes two arguments as integers.  These two integers
will be used as starting and ending points, for which to remove entire segments
from the shell history.

For instance, using `histdel 501 523` will remove all of the elements from
the shell history, starting at line number 501 and ending with line number 523.
These line numbers can be found with the `history` command in BASH.

## Download

    cd ~/
    git clone https://bitbucket.org/mdill/bash_history_delete.git

## Installing

It is suggested that this script be placed in `/usr/local/bin/` for simplicity
and ease-of-use.  Once this is done, it can be made executable by:

    $ chmod +x /usr/local/bin/{histdel,mostused}

Now, you may run:

    $ histdel [INTEGER] [INTEGER]

## License

This project is licensed under the BSD License - see the [LICENSE.md](https://bitbucket.org/mdill/bash_history_delete/src/d68e521430fa7d05c4733b7936fd85949f1d69ff/LICENSE.txt?at=master) file for
details.

